public class Arrangement {
    
    private int number;
    private String name;
    private String place;
    private String type;
    private String organizer;
    private Long dateAndTime;

    public Arrangement(int number, String name, String place, String type, String organizer, Long dateAndTime) {
        this.number = number;
        this.name = name;
        this.place = place;
        this.type = type;
        this.organizer = organizer;
        this.dateAndTime = dateAndTime;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getPlace() {
        return place;
    }

    public String getType() {
        return type;
    }

    public String getOrganizer() {
        return organizer;
    }

    public Long getDateAndTime() {
        return dateAndTime;
    }

    @Override
    public String toString() {
        return "Arrangement number: " + number + "\n" +
                "Name: " + name + "\n" +
                "Place: " + place + "\n" +
                "Type: " + type + "\n" +
                "Organizer: " + organizer + "\n" +
                "Date and time: " + dateAndTime + "\n";
    }
}
