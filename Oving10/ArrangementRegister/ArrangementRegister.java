import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

class SortByDateAndTime implements Comparator<Arrangement> {
    public int compare(Arrangement a, Arrangement b) {
        return (int) (a.getDateAndTime() - b.getDateAndTime());
    }
}

class SortByType implements Comparator<Arrangement> {
    public int compare(Arrangement a, Arrangement b) {
        return a.getType().compareToIgnoreCase(b.getType());
    }
}

class SortByPlace implements Comparator<Arrangement> {
    public int compare(Arrangement a, Arrangement b) {
        return a.getPlace().compareToIgnoreCase(b.getPlace());
    }
}

public class ArrangementRegister {
    ArrayList<Arrangement> arrangements = new ArrayList<>();

    public void newArrangement(String name, String place, String type, String organizer, Long dateAndTime) {
        Arrangement newArrangement = new Arrangement(arrangements.size()+1, name, place, type, organizer, dateAndTime);
        arrangements.add(newArrangement);
    }

    public ArrayList<Arrangement> findArrangementByPlace(String place) {
        place = place.toLowerCase();
        ArrayList<Arrangement> foundList = new ArrayList<>();
        Arrangement next;
        for (int i = 0; i < arrangements.size(); i++) {
            next = arrangements.get(i);
            if (next.getPlace().toLowerCase().equals(place)) {
                foundList.add(next);
            }
        }
        return foundList;
    }

    public ArrayList<Arrangement> findArrangementByDate(int date) {
        ArrayList<Arrangement> foundList = new ArrayList<>();
        int next;
        for (Arrangement arr: arrangements) {
            next = (int) (arr.getDateAndTime() / 10000);
            if (date == next) {
                foundList.add(arr);
            }
        }
        return foundList;
    }


    public ArrayList<Arrangement> findArrangementBetweenDates(int from, int to) {
        ArrayList<Arrangement> foundList = new ArrayList<>();
        int next;
        for (Arrangement arr: arrangements) {
            next = (int) (arr.getDateAndTime() / 10000);
            if (next >= from && next <= to) {
                foundList.add(arr);
            }
        }
        Collections.sort(foundList, new SortByDateAndTime());
        return foundList;
    }

    @Override
    public String toString() {
        String out = "Arrangements:\n\n";
        for (Arrangement arr: arrangements) {
            out += arr + "\n";
        }
        return out;
    }

    public ArrayList<Arrangement> listSortedByPlace() {
        ArrayList<Arrangement> sortedList = new ArrayList<>();
        sortedList.addAll(arrangements);
        Collections.sort(sortedList, new SortByPlace());
        return sortedList;
    }

    public ArrayList<Arrangement> listSortedByType() {
        ArrayList<Arrangement> sortedList = new ArrayList<>();
        sortedList.addAll(arrangements);
        Collections.sort(sortedList, new SortByType());
        return sortedList;
    }

    public ArrayList<Arrangement> listSortedByTime() {
        ArrayList<Arrangement> sortedList = new ArrayList<>();
        sortedList.addAll(arrangements);
        Collections.sort(sortedList, new SortByDateAndTime());
        return sortedList;
    }

    
    public static void main(String[] args) {
        ArrangementRegister ar = new ArrangementRegister();
        ar.newArrangement("Test1", "place1", "type1", "organizer1", 202301011000L);
        ar.newArrangement("Test2", "place2", "type2", "organizer2", 202301011100L);
        ar.newArrangement("Test3", "place1", "type3", "organizer1", 202301021000L);
        ar.newArrangement("Test4", "place2", "type2", "organizer2", 202302011100L);
        ar.newArrangement("Test5", "place3", "type3", "organizer3", 202302021000L);
        System.out.println(ar.findArrangementByDate(20230101).size());
        System.out.println(ar.findArrangementBetweenDates(20230101, 20230110).size());

        Scanner in = new Scanner(System.in);
        String menu =
                        "Menu:\n" +
                        "1: Register new arrangement\n" +
                        "2: Search for all arrangements at place\n" +
                        "3: Search for all arrangements on a date\n" +
                        "4: Search for all arrangements between two dates\n" +
                        "5: Print all\n" +
                        "6: Print all sorted by place, type and time\n" +
                        "0: Exit\n";
        boolean run = true;
        int choice;

        while (run) {
            try {
                System.out.println(menu);
                choice = Integer.parseInt(in.nextLine());
                switch (choice) {
                    case 0:
                        System.out.println("Shutting down...");
                        run = false;
                        break;
                    case 1:
                        System.out.println("Enter arrangement name:");
                        String name = in.nextLine();
                        System.out.println("Enter arrangement place:");
                        String place = in.nextLine();
                        System.out.println("Enter arrangement type:");
                        String type = in.nextLine();
                        System.out.println("Enter arrangement organizer:");
                        String organizer = in.nextLine();
                        System.out.println("Enter arrangment date and time (format: yyyymmddhhmm:");
                        Long dateAndTime = Long.parseLong(in.nextLine());
                        ar.newArrangement(name, place, type, organizer, dateAndTime);
                        System.out.println("New arrangement added");
                        break;
                    case 2:
                        System.out.println("Enter place:");
                        place = in.nextLine();
                        ArrayList<Arrangement> result = ar.findArrangementByPlace(place);
                        if (result.size() > 0) {
                            System.out.println("Found " + result.size() + " arrangements at " + place);
                            for (Arrangement arr: result) {
                                System.out.println(arr);
                            }
                        } else {
                            System.out.println("Found no arrangements at " + place);
                        }
                        break;
                    case 3:
                        System.out.println("Enter date (format: yyyymmdd)");
                        String inDate = in.nextLine();
                        result = ar.findArrangementByDate(Integer.parseInt(inDate));
                        if (result.size() > 0) {
                            System.out.println("\nFound " + result.size() + " arrangements on date " + inDate);
                            for (Arrangement arr: result) {
                                System.out.println(arr);
                            }
                        } else {
                            System.out.println("Found no arrangements on date " + inDate);
                        }
                        break;
                    case 4:
                        System.out.println("Enter from date (format: yyyymmdd)");
                        String fromDate = in.nextLine();
                        System.out.println("Enter to date (format: yyyymmdd)");
                        String toDate = in.nextLine();
                        result = ar.findArrangementBetweenDates(Integer.parseInt(fromDate), Integer.parseInt(toDate));
                        if (result.size() > 0) {
                            System.out.println("Found " + result.size() + " arrangements between dates " + fromDate + " and " + toDate);
                            for (Arrangement arr: result) {
                                System.out.println(arr);
                            }
                        } else {
                            System.out.println("Found no arrangements between dates " + fromDate + " and " + toDate);
                        }
                        break;
                    case 5:
                        System.out.println(ar);
                        break;
                    case 6:
                        System.out.println("\nSorted by time: ");
                        for (Arrangement arr: ar.listSortedByTime()) {
                            System.out.println(arr);
                        }
                        System.out.println("\nSorted by place: ");
                        for (Arrangement arr: ar.listSortedByPlace()) {
                            System.out.println(arr);
                        }
                        System.out.println("\nSorted by type: ");
                        for (Arrangement arr: ar.listSortedByType()) {
                            System.out.println(arr);
                        }
                        break;
                }
            } catch (Exception e) {
                System.out.println("Error: " + e);
            }

        }
        in.close();
    }
}