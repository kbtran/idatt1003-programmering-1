import java.util.ArrayList;

public class Meny {

    private ArrayList<Dish> dishes;
    private String name;

    public Meny(ArrayList<Dish> dishes, String name) {
        this.dishes = dishes;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Dish> getDishes() {
        return dishes;
    }

    public int getPrice() {
        int price = 0;
        for (Dish dish: dishes) {
            price += dish.getPrice();
        }
        return price;
    }

    @Override
    public String toString() {
        String out = "Menu name: " + name + "\n" +
                "Dishes: \n";
        for (Dish dish: dishes) {
            out += dish;
        }
        out += "\n";
        return out;
    }
}
