import java.util.ArrayList;
import java.util.Scanner;

public class MenyRegister {

    private String resturantName;
    private ArrayList<Dish> dishes = new ArrayList<>();
    private ArrayList<Meny> menus = new ArrayList<>();

    public MenyRegister(String resturantName) {
        this.resturantName = resturantName;
    }

    public ArrayList<Dish> getDishes() {
        return dishes;
    }

    public boolean newDish(String name, String type, double price, String recipe) {
        for (Dish dish : dishes) {
            if (dish.getName().equalsIgnoreCase(name)) {
                return false;
            }
        }
        Dish newDish = new Dish(name, type, price, recipe);
        dishes.add(newDish);
        return true;
    }

    public Dish getDishByName(String name) {
        for (Dish dish : dishes) {
            if (dish.getName().equalsIgnoreCase(name)) {
                return dish;
            }
        }
        return null;
    }

    public ArrayList<Dish> getDishByType(String type) {
        ArrayList<Dish> dishesOfType = new ArrayList<>();
        for (Dish dish : dishes) {
            if (dish.getType().equalsIgnoreCase(type)) {
                dishesOfType.add(dish);
            }
        }
        return dishesOfType;
    }

    public boolean newMenu(ArrayList<Dish> dishes, String name) {
        if (dishes.size() > 0) {
            menus.add(new Meny(dishes, name));
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String out = "Resturant name: " + resturantName + "\n\n";

        out += "MENUS: \n";
        for (Meny meny : menus) {
            out += meny;
        }

        out += "ALL DISHES: \n";
        for (Dish dish : dishes) {
            out += dish;
        }

        return out;
    }

    public ArrayList<Meny> getMenusWithinBudget(int min, int max) {
        ArrayList<Meny> menusWithinBudget = new ArrayList<>();
        int price = 0;
        for (Meny meny : menus) {
            price = meny.getPrice();
            if (price >= min && price <= max) {
                menusWithinBudget.add(meny);
            }
        }
        return menusWithinBudget;
    }

    public static void main(String[] args) {
        MenyRegister mr = new MenyRegister("Test Restaurant");
        mr.newDish("Dish 3", "Appetizer", 40, "Freeze");
        mr.newDish("Dish 1", "Main course", 30, "Fry");
        mr.newDish("Dish 5", "Dessert", 20, "Buy");
        ArrayList<Dish> meny1Dishes = new ArrayList<>();
        for (Dish d : mr.getDishes()) {
            meny1Dishes.add(d);
        }
        mr.newMenu(meny1Dishes, "Menu 1");
        mr.newDish("Dish 4", "Appetizer", 40, "Bake");
        mr.newDish("Dish 2", "Main course", 30, "Cook");
        mr.newDish("Dish 6", "Dessert", 20, "Buy");


        boolean run = true;
        Scanner in = new Scanner(System.in);
        String meny =
                        "Menu:\n" +
                        "1: Register new dish\n" +
                        "2: Find dish by name\n" +
                        "3: Find dishes by type\n" +
                        "4: Register new menu\n" +
                        "5: Find menu with a total price within interval\n" +
                        "6: Print all\n" +
                        "0: Exit\n";
        while (run) {
            try {
                System.out.println(meny);
                int choice = Integer.parseInt(in.nextLine());
                switch (choice) {
                    case 0:
                        System.out.println("Exited");
                        run = false;
                        break;
                    case 1:
                        System.out.println("Enter dish name:");
                        String name = in.nextLine();
                        System.out.println("Enter dish type:");
                        String type = in.nextLine();
                        System.out.println("Enter dish price:");
                        double price = Double.parseDouble(in.nextLine());
                        System.out.println("Enter dish recipe:");
                        String recipe = in.nextLine();
                        if (mr.newDish(name, type, price, recipe)) {
                            System.out.println("New dish registered.");
                        } else {
                            System.out.println("Dish not registered.");
                        }
                        break;
                    case 2:
                        System.out.println("Search for dish by name:");
                        name = in.nextLine();
                        Dish foundDish = mr.getDishByName(name);
                        if (foundDish != null) {
                            System.out.println("Dish found:\n" + foundDish);
                        } else {
                            System.out.println("Dish " + name + " not found.");
                        }
                        break;
                    case 3:
                        System.out.println("Search for dishes by type:");
                        type = in.nextLine();
                        ArrayList<Dish> foundDishes = mr.getDishByType(type);
                        if (foundDishes.size() > 0) {
                            System.out.println("Found " + foundDishes.size() + " dishes:");
                            for (Dish dish : foundDishes) {
                                System.out.println(dish);
                            }
                        } else {
                            System.out.println("Dishes with type " + type + " not found.");
                        }
                        break;
                    case 4:
                        boolean runMeny = true;
                        ArrayList<Dish> newDishes = new ArrayList<>();
                        System.out.println("Enter menu name: ");
                        String menuName = in.nextLine();
                        do {
                            System.out.println("Enter dish name:");
                            name = in.nextLine();
                            System.out.println("Enter dish type:");
                            type = in.nextLine();
                            System.out.println("Enter dish price:");
                            price = Double.parseDouble(in.nextLine());
                            System.out.println("Enter dish recipe:");
                            recipe = in.nextLine();
                            if (mr.newDish(name, type, price, recipe)) {
                                System.out.println("New dish registered.");
                                newDishes.add(new Dish(name, type, price, recipe));
                            } else {
                                System.out.println("Dish not registered.");
                            }
                            System.out.println("Enter 1 to add another dish. Enter 0 if done.");
                            choice = Integer.parseInt(in.nextLine());
                            switch (choice) {
                                case 1:
                                    System.out.println("New dish:");
                                    break;
                                case 0:
                                    mr.newMenu(newDishes, menuName);
                                    runMeny = false;
                                    break;
                                default:
                                    System.out.println("Invalid menu choice. Try again.");
                                    break;
                            }
                        } while (runMeny);
                        break;
                    case 5:
                        System.out.println("Enter minimum price:");
                        int min = Integer.parseInt(in.nextLine());
                        System.out.println("Enter maximum price:");
                        int max = Integer.parseInt(in.nextLine());
                        ArrayList<Meny> menusWithinBudget = mr.getMenusWithinBudget(min, max);
                        if (menusWithinBudget.size() > 0) {
                            System.out.println("Found " + menusWithinBudget.size() + " menus within interval:");
                            for (Meny m : menusWithinBudget) {
                                System.out.println(m);
                            }
                        } else {
                            System.out.println("No menus within interval.");
                        }
                        break;
                    case 6:
                        System.out.println(mr);
                        break;

                    default:
                        System.out.println("Invalid command");
                        break;
                }

            } catch (Exception e) {
                System.out.println("Error. " + e);
            }
        }
        in.close();
    }

}
