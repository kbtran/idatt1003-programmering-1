public class NyString {
    private String input;

    public NyString(String input) {
        this.input = input;
    }
    
    public String forkorting() {
        String[] strArray = input.split(" ");
        String forkortet = "";

        for(String i: strArray) {
            forkortet += i.charAt(0);
        }

        return forkortet;
    }

    public String fjernTegn(String... values) {

        String alteredString = input;

        for(String i: values) {
            alteredString = alteredString.replace(i, "");
        }

        return alteredString;

    }

    public static void main(String[] args) {
        NyString test = new NyString("Denne setningen kan forkortes");
        System.out.println(test.forkorting());
        System.out.println(test.fjernTegn("e", "s"));
    }
}
