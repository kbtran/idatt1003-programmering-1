import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class tekstBehandling {
    private String tekst;
    private static final String[] skilleTegn = {"!", ".", "?", ":"};

    public tekstBehandling(String tekst) {
        this.tekst = tekst;
    }

    public tekstBehandling(File file) throws FileNotFoundException{
        Scanner sc = new Scanner(file);

        String tekst = sc.nextLine();

        

        while(sc.hasNextLine()) {
            String line = sc.nextLine();
            tekst += " " + line;
        }
        
        sc.close();

        this.tekst = tekst;

    }

    public String getTekst() {
        return tekst;
    }

    public int antallOrd() {
        String[] ord = tekst.split(" ");
        int antOrd = ord.length;
        return antOrd;
    }

    public double gjennomsnittligOrdlengde() {
        String kopi = tekst;
        int numerator = 0;

        for(String i: skilleTegn) {
            kopi = kopi.replace(i, "");
        }

        String[] ord = kopi.split(" ");

        for(String i: ord) {
            numerator += i.length();
        }

        int denominator = ord.length;

        return (double) numerator/denominator;

    }

    public double gjennomsnittligAntOrdPerPeriode() {

        String[] kopi = tekst.split("[.!?:]");
        int denominator = kopi.length;
        int num = this.antallOrd();

        return (double) num/denominator;
    }

    public String endreOrd(String gammeltOrd, String nyttOrd) {
        String kopi = tekst.replace(gammeltOrd, nyttOrd);

        return kopi;
    }

    public String tekstIStorBokstav() {
        return tekst.toUpperCase();
    }

    public static void main(String[] args) throws FileNotFoundException{
        tekstBehandling a = new tekstBehandling("En to tre fire! fem Seks sju? åtte ni ti: Elleve tolv tretten fjorten en.");
        System.out.println(a.getTekst());
        System.out.println(a.antallOrd());
        System.out.println(a.gjennomsnittligOrdlengde());
        System.out.println(a.gjennomsnittligAntOrdPerPeriode());
        System.out.println(a.endreOrd("En", "Tre"));
        System.out.println(a.tekstIStorBokstav());

        File tekst = new File("blabla.txt");
        tekstBehandling b = new tekstBehandling(tekst);
        System.out.println(b.getTekst());
    }
    
}
