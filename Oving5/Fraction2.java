public class Fraction2 {
    private int numerator;
    private int denominator;

    public Fraction2(int numerator) {
        this.numerator = numerator;
        denominator = 1;
    }

    public Fraction2 (int numerator, int denominator) {
        if (denominator == 0) {
            throw new IllegalArgumentException("Numerator can not be 0.");
        }
        
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public String getFraction() {
        return numerator + "/" + denominator;
    }

    // Mathematical functions

    public void addFraction(Fraction2 otherFraction) {
        // Sum formula: a/b + c/d = (ad + cb)/bd
        int a = this.numerator;
        int b = this.denominator;
        int c = otherFraction.numerator;
        int d = otherFraction.denominator;

        numerator = a*d + c*b;
        denominator = b*d;
    }

    public void subtractFraction(Fraction2 otherFraction) {
        // Subtraction formula: a/b - c/d = (ad - cb)/bd
        int a = this.numerator;
        int b = this.denominator;
        int c = otherFraction.numerator;
        int d = otherFraction.denominator;

        numerator = a*d - c*b;
        denominator = b*d;
    }

    public void multiplyByFraction(Fraction2 otherFraction) {
        // Multiplication formula: a/b * c/d = ac/bd
        int a = this.numerator;
        int b = this.denominator;
        int c = otherFraction.numerator;
        int d = otherFraction.denominator;

        numerator = a*c;
        denominator = b*d;
    }

    public void divideByFraction(Fraction2 otherFraction) {
        // Division formula: (a/b) / (c/d) = ad/bc
        int a = this.numerator;
        int b = this.denominator;
        int c = otherFraction.numerator;
        int d = otherFraction.denominator;

        numerator = a*d;
        denominator = b*c;
    }

    public static void main(String[] args) {
        Fraction2 fraction1 = new Fraction2(5,2);
        Fraction2 fraction2 = new Fraction2(1,2);

        System.out.println("F1: " + fraction1.getFraction());
        System.out.println("F2: " + fraction2.getFraction() + "\n");

        fraction1.addFraction(fraction2);
        System.out.print("F1 + F2: ");
        System.out.print(fraction1.getFraction() + "\n");

        fraction2.addFraction(fraction1);
        System.out.print("F2 + F1: ");
        System.out.print(fraction2.getFraction() + "\n");

        fraction1.subtractFraction(fraction2);
        System.out.print("F1 - F2: ");
        System.out.print(fraction1.getFraction() + "\n");

        fraction1.multiplyByFraction(fraction2);
        System.out.print("F1 * F2: ");
        System.out.print(fraction1.getFraction() + "\n");

        fraction1.divideByFraction(fraction2);
        System.out.print("F1 / F2: ");
        System.out.print(fraction1.getFraction() + "\n");
    }


}

