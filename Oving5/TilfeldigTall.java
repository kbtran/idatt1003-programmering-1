import java.util.*;



public class TilfeldigTall {

    public static int[] TilfeldigTab(int b) {

        int[] intAntallForekomst = new int[10];

        for (int i = 0; i < b; i++) {

            java.util.Random random = new java.util.Random();
            int tall = random.nextInt(10);

            intAntallForekomst[tall] += 1;
        }
        return intAntallForekomst;

    }

    public static void main(String args[]) {

        int[] tabell1 = TilfeldigTab(1000);

        int totalt = 0;
        for (int i = 0; i < tabell1.length; i++) {
            System.out.println("Antall " + i + " = " + tabell1[i]);
            totalt += tabell1[i];
        }

        System.out.println("Totalt antall tall: " + totalt);
    }
}