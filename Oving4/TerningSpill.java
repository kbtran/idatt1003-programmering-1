import java.util.*;

class Player {

    private int sumPoeng;

    public Player() {
        sumPoeng = 0;
    }

    public int getSumPoeng() {
        return sumPoeng;
    }

    public void kastTerning() {
        java.util.Random terning = new java.util.Random();
        int terningkast = terning.nextInt(6) + 1;
        if(terningkast == 1) {
            sumPoeng = 0;
        }

        else {
            sumPoeng += terningkast;
        }

    }

    public boolean erFerdig() {
        if(sumPoeng >= 100) {
            return true;
        }

        return false;
    }
    
}


public class TerningSpill {
    public static void main(String[] args) {
        Player player1 = new Player();
        Player player2 = new Player();

        int i = 1;


        while(player1.erFerdig() != true && player2.erFerdig() != true) {
            player1.kastTerning();
            player2.kastTerning();
            System.out.println("Rundenummer: " + i);
            System.out.println("Poengsum til spiller 1: " + player1.getSumPoeng());
            System.out.println("Poengsum til spiller 2: " + player2.getSumPoeng());
            System.out.println();
            i++;
        }

    }
}
