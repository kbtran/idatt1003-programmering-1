import java.util.*;

class Valuta {
    private float valuta;

    public Valuta(float valuta) {
        this.valuta = valuta;
    }

    public float nokToDollar() {
        return valuta*0.092;
    }

    public float nokToEuro() {
        return valuta*0.087;
    }

    public float nokToSek() {
        return valuta*1.03;
    }

    public float dollarToNok() {
        return valuta*10.81;
    }

    public float euroToNok() {
        return valuta*11.54;
    }

    public float sekToNok() {
        return valuta*0.97;
    }

}

public class ValutaTest {
    public static void main(String[] args) {
        int a;
        float valuta;

        System.out.println("Velg valuta: ");
        System.out.print("1: dollar");
        System.out.println("2: euro");
        System.out.println("3: svenske kroner");
        System.out.println("Avslutt");
        Scanner myObj = new Scanner(System.in);
        a = myObj.nextInt();

        while(a != 0) {
            if(a==1) {
                System.out.println("Skriv inn antall dollar du vil konvertere");
                valuta = myObj.nextFloat();
                Valuta dollar = new Valuta(valuta);
                System.out.print(valuta + "dollar er lik:" + dollar.dollarToNok() + "norske kroner.");
            }

            if(a==2) {
                System.out.println("Skriv inn antall euro du vil konvertere");
                valuta = myObj.nextFloat();
                Valuta euro = new Valuta(valuta);
                System.out.print(valuta + "euro er lik:" + euro.euroToNok() + "norske kroner.");
            }

            if(a==3) {
                System.out.println("Skriv inn antall svenske kroner du vil konvertere");
                valuta = myObj.nextFloat();
                Valuta sek = new Valuta(valuta);
                System.out.print(valuta + "svenske kroner er lik:" + sek.sekToNok() + "norske kroner.");

            }

            else {
                System.out.println("Ugyldig input. Skriv inn på nytt");
            }

            System.out.println("Velg valuta: ");
            System.out.print("1: dollar");
            System.out.println("2: euro");
            System.out.println("3: svenske kroner");
            System.out.println("Avslutt");
            a = myObj.nextInt();
        }

        System.out.println("Avslutter programmet");
        myObj.close();
    }
}
