import java.util.Scanner;

public class MultiplicationTable {

    public static void multiTable(int start, int end) {
        for (int i = start; i < end+1; i++) {
            System.out.println();
            System.out.println(i+"-gangen:");
            for (int j = 1; j < 11; j++) {
                System.out.println(i*j);
            }
        };

    }
    public static void main(String args[]) {

        Scanner multiplicationTable = new Scanner(System.in);

        System.out.println("Enter startnumber and then endnumber: ");
        int tallStart = multiplicationTable.nextInt();

        int tallSlutt = multiplicationTable.nextInt();
        multiplicationTable.close();

        multiTable(tallStart, tallSlutt);
    }
}
