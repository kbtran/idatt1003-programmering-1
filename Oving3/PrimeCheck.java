import java.util.Scanner;

public class PrimeCheck {
    public static void primeCheck(int number) {
        boolean divisible = false;
            for (int i = 2; i <= number / 2; ++i) {
                if (number % i == 0) {
                    divisible = true;
                    break;
                }
             }

            if (!divisible && number != 1)
                System.out.println(number + " is a prime number.\n");
            else
                System.out.println(number + " is not a prime number.\n");

    }
    
    public static void main(String args[]) {
        
        Scanner number = new Scanner(System.in);
        int testNumber = 1;

        while (testNumber != 0) {
            System.out.println("Enter a number to check if it's prime: ");
            testNumber = number.nextInt();

            primeCheck(testNumber);

        }

        number.close();

    }
}
