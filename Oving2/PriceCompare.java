import java.util.Scanner;

public class PriceCompare {
    
    public static String compare(double weightA, double weightB) {
        final double pricePerGramA = 35.90/450;
        final double pricePerGramB = 39.50/500;

        if (pricePerGramA*weightA < pricePerGramB*weightB) {
            return "product A is cheaper";
        }

        if (pricePerGramA*weightA > pricePerGramB*weightB) {
            return "product B is cheaper";
        }
        
        else {
            return "The prices are equal";
        }
    }

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.print("Enter weight of product A in gram:");
        double weightA = input.nextDouble();

        System.out.print("Enter weight of product B in gram:");
        double weightB = input.nextDouble();

        System.out.print(compare(weightA, weightB));

        input.close();
    }

    
}
