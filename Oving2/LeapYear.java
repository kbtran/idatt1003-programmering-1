import java.util.Scanner;

public class LeapYear {

    public static boolean leapYear(int year) {
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
            return true;
        }

        return false;
    }
    public static void main(String[] args) {
        Scanner myYear = new Scanner(System.in);
        System.out.println("Enter year to check if it's a leap year:");

        int inputYear = myYear.nextInt();
        System.out.println(leapYear(inputYear));
        System.out.println("\n");

        myYear.close();
    }
    
}
