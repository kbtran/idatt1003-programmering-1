import java.util.Scanner;

public class ArbTaker {
    private Person personalia;
    private int arbtakernr;
    private int ansettelsesaar;
    private double maanedslonn;
    private double skatteprosent;

    public ArbTaker(Person personalia, int arbtakernr, int ansettelsesaar, double maanedslonn, double skatteprosent) {
        this.personalia = personalia;
        this.arbtakernr = arbtakernr;
        this.ansettelsesaar = ansettelsesaar;
        this.maanedslonn = maanedslonn;
        this.skatteprosent = skatteprosent;
    }

    public void setMaanedslonn(double maanedslonn) {
        this.maanedslonn = maanedslonn;
    }

    public void setSkatteprosent(double skatteprosent) {
        this.skatteprosent = skatteprosent;
    }

    public String getNavn() {
        return personalia.getEtternavn() + ", " + personalia.getFornavn();
    }

    public double skattPerMaaned() {
        return maanedslonn*skatteprosent;
    }

    public double bruttolonn() {
        return maanedslonn*12;
    }

    public double skattPerAar() {
        return maanedslonn*skatteprosent*10.5;
    }

    public int getAlder() {
        java.util.GregorianCalendar kalender = new java.util.GregorianCalendar();
        int år = kalender.get(java.util.Calendar.YEAR);

        return år-personalia.getFodselsaar();
    }

    public int antAarAnsatt() {
        java.util.GregorianCalendar kalender = new java.util.GregorianCalendar();
        int år = kalender.get(java.util.Calendar.YEAR);

        return år-ansettelsesaar;
    }

    public boolean merEnnAntAarAnsatt(int antAar) {
        if(this.antAarAnsatt() >= antAar) {
            return true;
        }

        return false;
    }

    public static void main(String[] args) {
        Person a = new Person("Ola", "Nordmann", 1990);

        ArbTaker a1 = new ArbTaker(a, 1, 2005, 50000, 0.30);

        System.out.println("Alder: " + a1.getAlder());
        System.out.println("Bruttolønn: " + a1.bruttolonn());
        System.out.println("Navn: " + a1.getNavn());
        System.out.println("Skatt per måned: " + a1.skattPerMaaned());
        System.out.println("Skatt per år: " + a1.skattPerAar());
        System.out.println("Antall år ansatt: " + a1.antAarAnsatt());
        System.out.println("Mer enn 10 år ansatt?: "+ a1.merEnnAntAarAnsatt(10));

        
        Scanner sc = new Scanner(System.in);
        System.out.println("\n");
        System.out.println("Meny:");
        System.out.println("Tast 1 for å endre månedslønn");
        System.out.println("Tast 2 for å endre skatteprosent");
        int input = sc.nextInt();

        while(input != 0) {
            if(input == 1) {
                System.out.println("Tast inn ny månedslønn");
                double nyLønn = sc.nextDouble();

                a1.setMaanedslonn(nyLønn);
                System.out.println("Ny månedslønn er: " + nyLønn);
            }

            if(input == 2) {
                System.out.println("Tast inn ny skatteprosent (mellom 0-1)");
                double nySkatt = sc.nextDouble();

                a1.setSkatteprosent(nySkatt);
                System.out.println("Ny skatteprosent er: " + nySkatt);
            }

            System.out.println("\n");
            System.out.println("Meny:");
            System.out.println("Tast 1 for å endre månedslønn");
            System.out.println("Tast 2 for å endre skatteprosent");
            input = sc.nextInt();
        
        }

        sc.close();
    }
}
